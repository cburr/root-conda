#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

PYTHON_VERSION=$1

conda create --yes --name "testenv-${PYTHON_VERSION}" "python=${PYTHON_VERSION}" nomkl root
set +u
source activate "testenv-${PYTHON_VERSION}"
set -u

root -l -b -q -x root/test.cpp

root -l -b -q -x root/test.cpp++
